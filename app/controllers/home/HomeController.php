<?php

use Entity\Applicant\ApplicantService;
use Entity\Application\ApplicationService;
use Entity\EmploymentInfo\EmploymentInfoService;

class HomeController
{

	private $applicationService;
	private $applicantService;
	private $employmentInfoService;
	private $reviewerService;

	public function __construct(
		ApplicationService $applicationService,
		ApplicantService $applicantService,
		EmploymentInfoService $employmentInfoService,
		ReviewerService $reviewerService)
	{
		$this->applicationService = $applicationService;
		$this->applicantService = $applicantService;
		$this->employmentInfoService = $employmentInfoService;
		$this->reviewerService = $reviewerService;
	}

	public function index(array $request)
	{
		$applications = $this->applicationService->getAll();
		$viewModel = $this->buildViewModel($applications);

		return $viewModel;
	}

	public function search(array $request)
	{
		$applications = $this->applicationService->getManyByEmail($request['email']);
		$viewModel = $this->buildViewModel($applications);

		return $viewModel;
	}

	private function buildViewModel(array $applications)
	{
		$applicants = $this->applicantService->getManyByApplications($applications);
		$employmentInfos = $this->employmentInfoService->getManyByApplicants($applicants);
		$reviewers = $this->reviewerService->getManyByApplications($applications);

		$viewModel = [
			'applications'=>[],
		];

		foreach ($applications as $application) {
			$applicant = $applicants[$application->getId()];

			$application = [
				'approved'=>$application->getIsApproved(),
				'applicant_name'=>$applicant->getName(),
				'employers'=>[],
				'reviewers'=>[],
			];

			foreach ($employmentInfos[$applicant->getId()] as $employmentInfo) {
				$application['employers'][] = $employmentInfo->getEmployerName();
			}

			foreach ($reviewers[$application->getId()] as $reviewer) {
				$application['reviewers'][] = $reviewer->getName();
			}

			$viewModel['applications'][] = $application;
		}

		return $viewModel;
	}

}