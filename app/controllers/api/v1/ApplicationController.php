<?php

use Entity\Application\Application;
use Entity\Application\ApplicationService;
use Transaction\TransactionResult;
use Transaction\TransactionService;

class ApplicationController
{

	private $applicationService;
	private $TransactionService;

	public function __construct(ApplicationService $applicationService, TransactionService $transactionService)
	{
		$this->applicationService = $applicationService;
		$this->transactionService = $transactionService;
	}

	public function index(array $request)
	{
		$applications = $this->applicationService->getAll();

		$viewModel = [
			'applications'=>[],
		];

		foreach ($applications as $application) {
			$viewModel['applications'][] = [
				'id'=>$application->getId(),
				'date_created'=>$application->getDateCreated(),
				'date_modified'=>$application->getDateModified(),
				'is_approved'=>$application->getIsApproved(),
			];
		}

		return $viewModel;
	}

	public function create(array $request)
	{
		return $this->transactionService->run(function() use ($request) {
			$application = new Application();
			$application->setIsApproved($request['is_approved']);

			$result = new TransactionResult($this->applicationService->persist($application));
			$result->setData($application);

			return $result;
		});
	}

}
