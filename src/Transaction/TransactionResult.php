<?php namespace Transaction;

class TransactionResult
{

	private $successful;
	private $data;

	public static function newResult($successful)
	{
		$result = new TransactionResult();
		return $result->setSuccessful($successful);
	}

	public function setSuccessful($successful)
	{
		$this->successful = $successful;

		return $this;
	}

	public function getSuccessful()
	{
		return $this->successful;
	}

	public function setData($data)
	{
		$this->data = $data;

		return $this;
	}

	public function getData()
	{
		return $this->data;
	}

	private function __construct() {
	}
	
}
