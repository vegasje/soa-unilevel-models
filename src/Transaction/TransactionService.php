<?php namespace Transaction;

class TransactionService
{

	private $dao;

	public function __construct(TransactionDao $dao)
	{
		$this->dao = $dao;
	}

	public function start()
	{
		return $this->dao->start();
	}

	public function rollback()
	{
		return $this->dao->rollback();
	}

	public function commit()
	{
		return $this->dao->commit();
	}

	public function run(callable $callable)
	{
		$this->dao->start();
		$result = $callable();

		if (!($result instanceof TransactionResult)) {
			return;
		}

		if (!$result->getSuccessful()) {
			$this->dao->rollback();
		} else {
			$this->dao->commit();
		}

		return $result->getData();
	}

}
