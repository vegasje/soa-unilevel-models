<?php namespace Transaction;

class TransactionDao
{

	private $pdo;

	public function __construct(PDO $pdo)
	{
		$this->pdo = $pdo;
	}

	public function start()
	{
		$this->pdo->beginTransaction();
	}

	public function rollback()
	{
		$this->pdo->rollback();
	}

	public function commit()
	{
		$this->pdo->commit();
	}

}
