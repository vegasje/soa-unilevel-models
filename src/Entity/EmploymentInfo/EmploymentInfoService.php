<?php namespace Entity\EmploymentInfo;

use Entity\EntityService;

class EmploymentInfoService extends EntityService
{

	/**
	 * getManyByApplicants gets all employment infos associated with the supplied applicants.
	 * @param  array  $applicants Single-dimensional array of applicant objects.
	 * @return array              Multi-dimensional array of employment info objects, keyed by the applicant's id.
	 */
	public function getManyByApplicants(array $applicants)
	{
		return $this->dao->getManyByApplicants($applicants);
	}

}