<?php namespace Entity\EmploymentInfo;

use Entity\EntityDao;
use PDO;

class EmploymentInfoDao extends EntityDao
{

	public function __construct(PDO $pdo)
	{
		parent::__construct($pdo, 'employment_infos');
	}

	/**
	 * getManyByApplicants gets all employment infos associated with the supplied applicants.
	 * @param  array   $applicants Single-dimensional array of applicant objects.
	 * @param  array   $properties Key-value pairs of properties and values used to filter results.
	 * @param  integer $limit      Max number of results to retrieve.
	 * @return array               Multi-dimensional array of employment info objects, keyed by the applicant's id.
	 */
	public function getManyByApplicants(array $applicants, array $properties=[], $limit=null)
	{
		return $this->getManyBy1ToM($applicants, 'applicant_id', function($applicant) {
			return $applicant->getId();
		}, $properties, $limit);
	}

	private function hydrate(array $data=null)
	{
		if ($data === null) {
			return null;
		}

		$employmentInfo = new EmploymentInfo();

		parent::hydrateBase($employmentInfo, $data);
		$employmentInfo->setEmployerName($data['employer_name']);
		$employmentInfo->setApplicationId($data['applicant_id']);

		return $employmentInfo;
	}

}
