<?php namespace Entity\EmploymentInfo;

use Entity\Entity;

/**
 * EmploymentInfo represents employment information for an Applicant.
 *
 * EmploymentInfo has a M->1 relationship with Applicant.
 */
class EmploymentInfo extends Entity
{

	private $applicantId;
	private $employerName;

	public function getApplicantId()
	{
		return $this->applicantId;
	}

	public function setApplicantId($applicantId)
	{
		$this->applicantId = $applicantId;
	}

	public function getEmployerName()
	{
		return $this->employerName;
	}

	public function setEmployerName($employerName)
	{
		$this->employerName = $employerName;
	}

}