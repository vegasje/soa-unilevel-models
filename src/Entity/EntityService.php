<?php namespace Entity;

abstract class EntityService
{

	protected $dao;

	public function __construct(EntityDao $dao)
	{
		$this->dao = $dao;
	}

	/**
	 * getAll gets all of the entities from the database.
	 * @param  boolean $hideDeleted True if soft-deleted entities should be ignored.
	 * @return array                Single-dimensional array of entities.
	 */
	public function getAll($hideDeleted=true)
	{
		return $this->dao->getAll($hideDeleted);
	}

	/**
	 * getOneById gets a single entity from the database based on the primary identifier.
	 * @param  integer/string   $id Primary identifier from the database.
	 * @return ? extends Entity     Single entity.
	 */
	public function getOneById($id)
	{
		return $this->dao->getOneById($id);
	}

}