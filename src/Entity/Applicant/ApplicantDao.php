<?php namespace Entity\Applicant;

use Entity\EntityDao;
use PDO;

class ApplicantDao extends EntityDao
{

	public function __construct(PDO $pdo)
	{
		parent::__construct($pdo, 'applicants');
	}

	/**
	 * getManyByApplications gets all applicants associated with the supplied applications.
	 * @param  array   $applications Single-dimensional array of application objects.
	 * @param  array   $properties   Key-value pairs of properties and values used to filter results.
	 * @param  integer $limit        Max number of results to retrieve.
	 * @return array                 Single-dimensional array of applicant objects, keyed by the application's id.
	 */
	public function getManyByApplications(array $applications, array $properties=[], $limit=null)
	{
		return $this->getManyBy1To1($applications, 'application_id', function($application) {
			return $application->getId();
		}, $properties, $limit);
	}

	/**
	 * getManyByEmploymentInfos gets all applicants associated with the supplied employment infos.
	 * @param  array   $employmentInfos Single-dimensional array of employment inof objects.
	 * @param  array   $properties      Key-value pairs of properties and values used to filter results.
	 * @param  integer $limit           Max number of results to retrieve.
	 * @return array                    Single-dimensional array of applicant objects, keyed by the applicant's id.
	 */
	public function getManyByEmploymentInfos(array $employmentInfos, array $properties=[], $limit=null)
	{
		return $this->getManyByMTo1($employmentInfos, 'id', function($employmentInfo) {
			return $employmentInfo->getApplicantId();
		}, $properties, $limit);
	}

	private function hydrate(array $data=null)
	{
		if ($data === null) {
			return null;
		}

		$applicant = new Applicant();

		parent::hydrateBase($applicant, $data);
		$applicant->setName($data['name']);
		$applicant->setApplicationId($data['application_id']);

		return $applicant;
	}

}
