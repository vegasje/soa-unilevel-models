<?php namespace Entity\Applicant;

use Entity\EntityService;

class ApplicantService extends EntityService
{

	/**
	 * getManyByApplications gets all applicants associated with the supplied applications.
	 * @param  array  $applications Single-dimensional array of application objects.
	 * @return array                Single-dimensional array of applicant objects, keyed by the application's id.
	 */
	public function getManyByApplications(array $applications)
	{
		return $this->dao->getManyByApplications($applications);
	}

	/**
	 * getManyByEmploymentInfos gets all applicants associated with the supplied employment infos.
	 * @param  array  $employmentInfos Single-dimensional array of employment inof objects.
	 * @return array                   Single-dimensional array of applicant objects, keyed by the applicant's id.
	 */
	public function getManyByEmploymentInfos(array $employmentInfos)
	{
		return $this->dao->getManyByEmploymentInfos($employmentInfos);
	}

}