<?php namespace Entity\Applicant;

use Entity\Entity;

/**
 * Applicant represents and individual that has submitted an application.
 *
 * Applicant has a 1->1 relationship with Application.
 * Applicant has a 1->M relationship with EmploymentInfo.
 */
class Applicant extends Entity
{

	private $applicationId;
	private $name;

	public function getApplicationId()
	{
		return $this->applicationId;
	}

	public function setApplicationId($applicationId)
	{
		$this->applicationId = $applicationId;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

}