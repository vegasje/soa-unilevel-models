<?php namespace Entity;

abstract class Entity
{

	protected $id;
	protected $dateCreated;
	protected $dateModified;
	protected $dateDeleted;

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getDateCreated()
	{
		return $this->dateCreated;
	}

	public function setDateCreated($dateCreated)
	{
		$this->dateCreated = $dateCreated;
	}

	public function getDateModified()
	{
		return $this->dateModified;
	}

	public function setDateModified($dateModified)
	{
		$this->dateModified = $dateModified;
	}

	public function getDateDeleted()
	{
		return $this->dateDeleted;
	}

	public function setDateDeleted($dateDeleted)
	{
		$this->dateDeleted = $dateDeleted;
	}

	public function isDeleted()
	{
		return (!empty($dateDeleted));
	}

}