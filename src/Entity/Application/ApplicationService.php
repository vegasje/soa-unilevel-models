<?php namespace Entity\Application;

use Entity\EntityService;

class ApplicationService extends EntityService
{

	/**
	 * getManyByEmail gets any applications where the applicant has the supplied email address.
	 * @param  string $email Applicant email address.
	 * @return array         Single-dimensional array of application objects.
	 */
	public function getManyByEmail($email)
	{
		return $this->dao->getManyByEmail($email);
	}

}