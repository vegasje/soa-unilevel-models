<?php namespace Entity\Application;

use Entity\EntityDao;
use PDO;

class ApplicationDao extends EntityDao
{

	public function __construct(PDO $pdo)
	{
		parent::__construct($pdo, 'applications');
	}

	/**
	 * getManyByEmail gets any applications where the applicant has the supplied email address.
	 * @param  string $email Applicant email address.
	 * @return array         Single-dimensional array of application objects.
	 */
	public function getManyByEmail($email)
	{
		return $this->getManyByProperties(['email'=>$email]);
	}

	private function hydrate(array $data=null)
	{
		if ($data === null) {
			return null;
		}

		$application = new Application();

		parent::hydrateBase($application, $data);
		$application->setIsApproved($data['is_approved']);

		return $application;
	}

}
