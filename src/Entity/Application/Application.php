<?php namespace Entity\Application;

use Entity\Entity;

/**
 * Application represents an application submitted to the system.
 *
 * Application has a 1->1 relationship with Applicant.
 * Application has a M->M relationship with Reviewer.
 */
class Application extends Entity
{

	private $isApproved;

	public function getIsApproved()
	{
		return $this->isApproved;
	}

	public function setIsApproved($isApproved)
	{
		$this->isApproved = $isApproved;
	}

}