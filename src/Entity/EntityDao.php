<?php namespace Entity;

use PDO;

abstract class EntityDao
{

	protected $pdo;
	protected $tableName;

	public function __construct(PDO $pdo, $tableName)
	{
		$this->pdo = $pdo;
		$this->tableName = $tableName;
	}

	/**
	 * hydrate takes the raw array data returned from PDO and turns it into a model.
	 * This must be implemented by all subclasses.
	 * @param  array            $data Raw PDO data in associative array format.
	 * @return ? extends Entity       Model object.
	 */
	abstract protected function hydrate(array $data);

	/**
	 * getAll gets all of the entities from the database.
	 * @param  boolean $hideDeleted True if soft-deleted entities should be ignored.
	 * @return array                Single-dimensional array of entities.
	 */
	protected function getAll($hideDeleted=true)
	{
		$rows = $this->getAllArray($hideDeleted);

		return $this->hydrateAll($rows);
	}

	/**
	 * getOneById gets a single entity from the database based on the primary identifier.
	 * @param  integer/string   $id Primary identifier from the database.
	 * @return ? extends Entity     Single entity.
	 */
	protected function getOneById($id)
	{
		return $this->getOneByProperties(['id'=>$id]);
	}

	/**
	 * getOneByProperties gets a single entity from the database based on a set of properties.
	 * @param  array            $properties Key-value pairs of properties and values used to filter results.
	 * @return ? extends Entity             Single entity.
	 */
	protected function getOneByProperties(array $properties)
	{
		return $this->getManyByProperties($properties, 1);
	}

	/**
	 * getManyByProperties gets any entities from the database based on a set of properties.
	 * @param  array   $properties Key-value pairs of properties and values used to filter results.
	 * @param  integer $limit      Max number of results to retrieve.
	 * @return array               Single-dimentional array of entities.
	 */
	protected function getManyByProperties(array $properties, $limit=null)
	{
		$rows = $this->getManyArrayByProperties($properties, $limit);

		return $this->hydrateAll($rows);
	}

	/**
	 * getManyBy1To1 gets associated entities in a 1->1 relationship with the supplied entities.
	 * @param  array    $objects  Single-dimensional array of entities.
	 * @param  string   $column   Column on the local table to use when querying.
	 * @param  callable $callable Map function that receives an individual object and returns the id to use in the query.
	 * @return array              Multi-dimensional array of entities, keyed by the foreign id.
	 */
	protected function getManyBy1To1(array $objects, $column, callable $callable, array $properties=[], $limit=null)
	{
		$rows = $this->getManyArrayByRelationship($objects, $column, $callable, $properties, $limit);

		return $this->hydrateAll($rows, $column);
	}

	/**
	 * getManyByMTo1 gets associated entities in a M->1 relationship with the supplied entities.
	 * @param  array    $objects  Single-dimensional array of entities.
	 * @param  string   $column   Column on the local table to use when querying.
	 * @param  callable $callable Map function that receives an individual object and returns the id to use in the query.
	 * @return array              Multi-dimensional array of entities, keyed by the foreign id.
	 */
	protected function getManyByMTo1(array $objects, $column, callable $callable, array $properties=[], $limit=null)
	{
		$rows = $this->getManyArrayByRelationship($objects, $column, $callable, $properties, $limit);

		return $this->hydrateAll($rows, $column);
	}

	/**
	 * getManyBy1ToM gets associated entities in a 1->M relationship with the supplied entities.
	 * @param  array    $objects  Single-dimensional array of entities.
	 * @param  string   $column   Column on the local table to use when querying.
	 * @param  callable $callable Map function that receives an individual object and returns the id to use in the query.
	 * @return array              Multi-dimensional array of entities, keyed by the foreign id.
	 */
	protected function getManyBy1ToM(array $objects, $column, callable $callable, array $properties=[], $limit=null)
	{
		$rows = $this->getManyArrayByRelationship($objects, $column, $callable, $properties, $limit);

		return $this->hydrateAll($rows, $column, true);
	}

	/**
	 * getManyByMToM gets associated entities in a M->M relationship with the supplied entities.
	 * This involves a junction table that contains primary keys from the related entities.
	 * @param  array    $objects        Single-dimensional array of entities.
	 * @param  string   $junctionTable  Name of the junction table.
	 * @param  string   $localColumn    Name of the local column.
	 * @param  string   $junctionColumn Name of the junction column.
	 * @param  string   $foreignColumn  Name of the foreign column.
	 * @param  callable $callable       Map function that receives and individual object and returns the id to use in the query.
	 * @return array                    Multi-dimensional array of entities, keyed by the foreign id.
	 */
	protected function getManyByMToM(
			array $objects,
			$junctionTable, $localColumn, $junctionColumn, $foreignColumn,
			callable $callable,
			array $properties=[], $limit=null)
	{
		$rows = $this->getManyArrayByRelationshipThrough(
				$objects, $junctionTable, $localColumn, $junctionColumn, $foreignColumn, $callable, $properties, $limit);

		return $this->hydrateAll($rows, $foreignColumn, true);
	}

	protected function getAllArray($hideDeleted=true)
	{
		$sql = "
			SELECT *
			FROM `{$this->tableName}`
		";

		if ($hideDeleted) {
			$sql .= "
				WHERE `date_deleted` IS NULL
			";
		}

		$sql .= ";";

		$statement = $this->pdo->prepare($sql);
		$statement->execute();

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	protected function getManyArrayByRelationship(array $objects, $column, callable $callable, array $properties=[], $limit=null)
	{
		$values = array_map($callable, $objects);
		$in = $this->buildIn($values);

		$sql = "
			SELECT *
			FROM `{$this->tableName}`
			WHERE `{$column}` IN ({$in})
		";

		if (!empty($properties)) {
			$where = implode(' AND ', array_map(function($property) {
				return "`{$property}` = :{$property}";
			}, array_keys($properties)));

			$sql .= "
				AND {$where}
			";
		}

		if ($limit !== null && is_int($limit)) {
			$sql .= "
				LIMIT {$limit}
			";
		}

		$sql .= ";";

		$statement = $this->pdo->prepare($sql);
		$statement->execute($properties);

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	protected function getManyArrayByRelationshipThrough(
			array $objects,
			$junctionTable,
			$localColumn, $junctionColumn, $foreignColumn,
			callable $callable,
			array $properties=[], $limit=null)
	{
		$values = array_map($callable, $objects);
		$in = $this->buildIn($values);

		$sql = "
			SELECT
				`{$this->tableName}`.*,
				`{$junctionTable}`.`{$foreignColumn}`
			FROM
				`{$this->tableName}`
				JOIN `{$junctionTable}` ON `{$junctionTable}`.`{$junctionColumn}` = `{$this->tableName}`.`{$localColumn}`
			WHERE `{$junctionTable}`.`{$foreignColumn}` IN ({$in})
		";

		if (!empty($properties)) {
			$where = implode(' AND ', array_map(function($property) {
				return "`{$property}` = :{$property}";
			}, array_keys($properties)));

			$sql .= "
				AND {$where}
			";
		}

		if ($limit !== null) {
			$sql .= "
				LIMIT {$limit}
			";
		}

		$sql .= ";";

		$statement = $this->pdo->prepare($sql);
		$statement->execute($properties);

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	protected function getManyArrayByProperties(array $properties, $limit=null)
	{
		$where = implode(' AND ', array_map(function($property) {
			return "`{$property}` = :{$property}";
		}, array_keys($properties)));

		$sql = "
			SELECT *
			FROM `{$this->tableName}`
			WHERE {$where}
		";

		if ($limit !== null && is_int($limit)) {
			$sql .= "
				LIMIT {$limit}
			";
		}

		$sql .= ";";

		$statement = $this->pdo->prepare($sql);
		$statement->execute($properties);

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	protected function hydrateAll(array $datas, $key=null, $isMulti=false)
	{
		$entitys = [];

		foreach ($datas as $data) {
			$entity = $this->hydrate($data);

			if ($key !== null) {
				if ($isMulti) {
					if (!isset($entitys[$data[$key]])) {
						$entitys[$data[$key]] = [];
					}

					$entitys[$data[$key]][] = $entity;
				} else {
					$entitys[$data[$key]] = $entity;
				}
			} else {
				$entitys[] = $entity;
			}
		}

		return $entitys;
	}

	protected function hydrateBase(Entity &$entity, array $data)
	{
		$entity->setId($data['id']);
		$entity->setDateCreated($data['date_created']);
		$entity->setDateModified($data['date_modified']);
		$entity->setDateDeleted($data['date_deleted']);
	}

	protected function buildIn(array $values)
	{
		return implode(',', $values);
	}

}
