<?php namespace Entity\Reviewer;

use Entity\Entity;

/**
 * Reviewer represents a person or service reviewing an application.
 *
 * Reviewer has a M->M relationship with Application.
 */
class Reviewer extends Entity
{

	private $name;

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

}