<?php namespace Entity\Reviewer;

use Entity\EntityService;

class ReviewerService extends EntityService
{

	/**
	 * getManyByApplications gets all reviewers associated with the supplied applications.
	 * @param  array  $applications Single-dimensional array of application objects.
	 * @return array                Multi-dimensional array of reviewer objects, keyed by the application's id.
	 */
	public function getManyByApplications(array $applications)
	{
		return $this->dao->getManyByApplications();
	}

}