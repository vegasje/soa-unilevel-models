<?php namespace Entity\Reviewer;

use Entity\EntityDao;
use PDO;

class ReviewerDao extends EntityDao
{

	public function __construct(PDO $pdo)
	{
		parent::__construct($pdo, 'reviewers');
	}

	/**
	 * getManyByApplications gets all reviewers associated with the supplied applications.
	 * @param  array   $applications Single-dimensional array of application objects.
	 * @param  array   $properties   Key-value pairs of properties and values used to filter results.
	 * @param  integer $limit        Max number of results to retrieve.
	 * @return array                 Multi-dimensional array of reviewer objects, keyed by the application's id.
	 */
	public function getManyByApplications(array $applications, array $properties=[], $limit=null)
	{
		return $this->getManyByMToM($applications, 'application_reviewers', 'id', 'reviewer_id', 'application_id', function($application) {
			return $application->getId();
		}, $properties, $limit);
	}

	public function hydrate(array $data=null)
	{
		if ($data === null) {
			return null;
		}

		$reviewer = new Reviewer();

		parent::hydrateBase($reviewer, $data);
		$reviewer->setName($data['name']);

		return $reviewer;
	}

}
